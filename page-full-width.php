<?php
/*
Template Name: Full Width (No Sidebar)
*/
?>

<?php get_header(); ?>
			
			<div id="content">
			
				<div id="inner-content" class="row clearfix">
			
				    <div id="main" class="large-12 medium-12 columns" role="main">
				    
				        <div id="biog" class="large-3 show-for-medium-up columns" role="aside">
				        Put biog info here
				        </div>
					
                        <div id="fp-wrapper" class="large-9 small-12 columns">
                        
<?php
        // Get the ID of a given category
        $category_id = get_cat_ID( 'Books' );

        // Get the URL of this category
        $category_link = get_category_link( $category_id );
?>

<div class="books large-4 medium-6 small-6 columns">
    <div class="cat-details">
    <h4>

    <!-- Print a link to this category -->
    <a href="<?php echo esc_url( $category_link ); ?>" title="<?php echo get_cat_name($category_id);?>"><?php echo get_cat_name($category_id);?></a> 
    </h4>
    <i class="fi-plus"></i>
    </div>
</div>

<?php
        // Get the ID of a given category
        $category_id = get_cat_id( 'Articles and Papers' );

        // Get the URL of this category
        $category_link = get_category_link( $category_id );
    ?>

<div class="articles large-4 medium-6 small-6 columns">
    <div class="cat-details">
   <h4>

    <!-- Print a link to this category -->
    <a href="<?php echo esc_url( $category_link ); ?>" title="<?php echo get_cat_name($category_id);?>"><?php echo get_cat_name($category_id);?></a> 
    </h4>
    <i class="fi-plus"></i>
    </div>
</div>


<?php
        // Get the ID of a given category
        $category_id = get_cat_ID( 'Research Projects' );

        // Get the URL of this category
        $category_link = get_category_link( $category_id );
?>

<div class="research large-4 medium-6 small-6 columns">
    <div class="cat-details">
    <h4>

    <!-- Print a link to this category -->
    <a href="<?php echo esc_url( $category_link ); ?>" title="<?php echo get_cat_name($category_id);?>"><?php echo get_cat_name($category_id);?></a> 
    </h4>
    <i class="fi-plus"></i>
    </div>
</div>
       
<?php
        // Get the ID of a given category
        $category_id = get_cat_ID( 'Teaching and Courses' );

        // Get the URL of this category
        $category_link = get_category_link( $category_id );
?>
    
<div class="teaching large-4 medium-6 small-6 columns">
    <div class="cat-details">
    <h4>

    <!-- Print a link to this category -->
    <a href="<?php echo esc_url( $category_link ); ?>" title="<?php echo get_cat_name($category_id);?>"><?php echo get_cat_name($category_id);?></a> 
    </h4>
    <i class="fi-plus"></i>
    </div>
</div>

<?php
        // Get the ID of a given category
        $category_id = get_cat_ID( 'Lectures' );

        // Get the URL of this category
        $category_link = get_category_link( $category_id );
?>
    
<div class="lectures large-4 medium-6 small-6 columns">
    <div class="cat-details">
    <h4>

    <!-- Print a link to this category -->
    <a href="<?php echo esc_url( $category_link ); ?>" title="<?php echo get_cat_name($category_id);?>"><?php echo get_cat_name($category_id);?></a> 
    </h4>
    <i class="fi-plus"></i>
    </div>
</div>

<?php
        // Get the ID of a given category
        $category_id = get_cat_ID( 'Networks' );

        // Get the URL of this category
        $category_link = get_category_link( $category_id );
?>
    
<div class="networks large-4 medium-6 small-6 columns">
    <div class="cat-details">
    <h4>

    <!-- Print a link to this category -->
    <a href="<?php echo esc_url( $category_link ); ?>" title="<?php echo get_cat_name($category_id);?>"><?php echo get_cat_name($category_id);?></a> 
    </h4>
   <i class="fi-plus"></i>
    </div>
</div>


   				        </div> <!-- end #fp-wrapper -->
    				</div> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
